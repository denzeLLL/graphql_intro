const express = require('express');
const server = express();

const api = require("./api");

server.get('/', (req,  res) => {
    res.send('hello world!');
});

server.use('/', api);

server.listen(3001, () => {
    console.log('test server on port 3001');
});