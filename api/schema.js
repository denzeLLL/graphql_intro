const { buildSchema } = require('graphql');
const path =  require('path');
const fs = require('fs');

const schema = fs.readFileSync(path.resolve(__dirname, 'schema.graphql'), 'utf-8');

module.exports = buildSchema(schema);
  