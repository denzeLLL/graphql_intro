const express = require('express');
const graphqlMiddleWare = require('express-graphql');
// импортируем схему в файле index.js 
const schema = require('./schema.js');


const mongoose = require('mongoose');
mongoose.Promise = Promise;
mongoose.connect('mongodb://localhost:27017/graphql-intro');
mongoose.connection.once('open', () => console.log('connect to MongoDB'));

// импортируем модель (mongoose)
const Pet = require('./model');

const api = express();


api.all('/api', graphqlMiddleWare({
    schema,
    graphiql: true,
    rootValue: {
        // 1 - название запроса; 2 - ф-я (реализация схемы)
        pet: ({id}) => Pet.findById(id)
        ,
        pets: () => Pet.find({}),
        createPet: ({ input }) => {
            // метод mongoose create возвращает Promise
            return Pet.create(input)
        },
        updatePet: ({ id, input }) => {
            return Pet.findByIdAndUpdate( id, input, {
                new: true // то есть вернуть новый объект
            })
        },
        deletePet: ({ id }) => {
            return Pet.deleteOne({_id: id}).then(() => {
                return id;
            })

        },
    }
}));

module.exports = api;