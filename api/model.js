const mongoose = require('mongoose');

const Pet = new mongoose.Schema({
    //id: - за id будет отвечать MongoDB
    name: {
        type: String,
        required: true
    },
    species: {
        type: String,
        required: true
    },
    favFoods: [String],
    birthYear: Number,
    photo: String,
    steps: [{
        title: String,
        completed: Boolean
    }]
});

module.exports = mongoose.model('Pet', Pet);